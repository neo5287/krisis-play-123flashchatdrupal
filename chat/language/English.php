<?php
/*
+ -----------------------------------------------------------------------------+
|     e107 website system - Language File.
|
|     $URL: https://e107.svn.sourceforge.net/svnroot/e107/trunk/e107_0.7/e107_plugins/pdf/languages/English.php $
|     $Revision: 11678 $
|     $Id: English.php 11678 2010-08-22 00:43:45Z e107coders $
|     $Author: e107coders $
+-----------------------------------------------------------------------------+
*/
define("CHAT_TITLE", "123 Flash Chat");
define("CHAT_DESC", "123 Flash Chat Module.");
define("CHAT_INSTALL_SUCCESSFULL", "123 Flash Chat Module has been installed successfully.");
define("CHAT_SERVER_MODE_CAPTION", "Chat Server Mode:");
define("CHAT_SERVER_MODE_CAPTION0", 'Chat Server Host By Own <span style="font-family: verdana,sans-serif;font-size: 0.85em;font-style: italic;"><br/>Please download and install 123FlashChat first: <a target="_blank" href="http://www.123flashchat.com/download.html"> http://www.123flashchat.com/download.html</a>, 
		<br/>and set Chat Client Location to: http://&lt;your chat server domain or ip&gt;:35555/</span>');

define("CHAT_SERVER_MODE_CAPTION1", 'Chat Server Host By 123 Flash Chat <span style="font-family: verdana,sans-serif;font-size: 0.85em;font-style: italic;"><br/><a target="_blank" href="http://www.123flashchat.com/host.html">For paid host</a>, please set the Chat Client Location like this, for example: http://host71200.123flashchat.com/wordpress/, <a target="_blank" href="http://www.123flashchat.com/host.html">Buy host</a>
   <br /><a target="_blank" href="http://www.123flashchat.com/host/apply.php">For trial host</a>, please setup Chat Client Location like this: http://trial.123flashchat.com/yourhostname
   <br />Just replace "yourhostname" to the real one when you applied, <a target="_blank" href="http://www.123flashchat.com/host/apply.php">Apply trial host</a></span>');
define("CHAT_SERVER_MODE_CAPTION2", 'Chat Server Host By 123 Flash Chat For Free<span style="font-family: verdana,sans-serif;font-size: 0.85em;font-style: italic;"><br />This chat server mode aims at testing the basic functions, only supported 1 room, no video chat function, and also you don\'t have the administrator permission of entering this chat room, you can select the mode 1 or 2 to get the full functions and control your chat room.</span>');
$desc = <<<DESC
It is highly recommended to copy the chat client folder and files to your web server to reduce chat server load. For example, copy <123flashchat server installed directory>/client/ to //chat/, and configure "Chat Client Location" to http://www.yourdomain.com/chat/(i.e., //chat/) accordingly.
DESC;
define("CHAT_CLIENT_DESC", $desc);
define("CHAT_SERVER_HOST_CAPTION", "Chat Server Host:");
define("CHAT_SERVER_PORT_CAPTION", "Chat Server Port:");
define("CHAT_HTTP_PORT_CAPTION", "Chat Http Port:");
define("CHAT_CLIENT_LOCATION_CAPTION", "Chat Client Location:");
define("CHAT_SHOW_ROOM_LIST_CAPTION", "Show rooms list:");
define("CHAT_SHOW_USER_LIST_CAPTION", "Show users list:");
define("CHAT_CLIENT_SIZE_CAPTION", "Chat Client Size:");
define("CHAT_CLIENT_SIZE_WIDTH", "Width:");
define("CHAT_CLIENT_SIZE_HEIGHT", "Height:");
define("CHAT_CLIENT_SIZE_FSC", "Full Screen");
define("CHAT_ROOM_CAPTION", "Chat Room:");

define("CHAT_CLIENT_LANGUAGE_CAPTION", "Chat Client Language:");
define("CHAT_CLIENT_SKIN_CAPTION", "Chat Client Skin:");
define("CHAT_CLIENT_LANGUAGE_NOTE", "Note: You can set the language in 123FlashChat Admin Panel -> Server Setting -> Language Setting");
define("CHAT_CLIENT_SKIN_NOTE", "Note: You can set the skin in 123FlashChat Admin Panel -> Client Setting -> Skin");
define("SAVE_SUCCESSFULLY","Save Successfully");
define("SAVE_FAILED","Save Failed");
define("CLIENT_ERROR","Client Location is configured incorrectly");
define("SERVER_ERROR","Chat Server host or http_port is configured incorrectly");


?>