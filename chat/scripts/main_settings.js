/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
function dE(id, s){
    var e = document.getElementById(id);
    if (!s){
        s = (e.style.display == '') ? -1 : 1;
    }
    e.style.display = (s == 1) ? '' : 'none';
}

function serverChange(s){
    switch(s){
        case "0":
            if(document.getElementById("fc_server_local").value == 1)
            {
                dE('s_host', 1);
                dE('s_port', 1);
                dE('s_http_port', 1);
                dE('freedownload', 1);
            }else
            {
                dE('s_host', -1);
                dE('s_port', -1);
                dE('s_http_port', -1);
                dE('freedownload', -1);
            }
            dE('s_fc', 1);
            dE('s_free', -1);
            dE('s_rl', 1);
            dE('s_info', 1);
            dE('u_as_lab', 1);
            dE('u_al_lab', 1);
            dE('fc_client_skin', -1);
            dE('fc_client_lang', -1);
            break;
        case "1":
            dE('s_host', -1);
            dE('s_port', -1);
            dE('s_http_port', -1);
            dE('freedownload', -1);
            dE('s_fc', 1);
            dE('s_free', -1);
            dE('s_rl', 1);
            dE('s_info', -1);
            dE('u_as_lab', 1);
            dE('u_al_lab', 1);
            dE('fc_client_skin', -1);
            dE('fc_client_lang', -1);
            break;
        case "2":

            dE('fc_client_skin', 1);
            dE('fc_client_lang', 1);
            dE('u_as_lab', -1);
            dE('u_al_lab', -1);
            dE('s_info', -1);
            dE('s_host', -1);
            dE('s_port', -1);
            dE('s_http_port', -1);
            dE('freedownload', -1);
            dE('s_fc', -1);
            dE('s_free', 1);
            dE('s_rl', -1);
            break;
    }
    var c = getFCClient();
    clientChange(c);
}

function clientChange(s){
    switch(s){
        case "0":
            dE('c_skin', 1);
            dE('lang', 1);
            break;
        case "1":
            dE('c_skin', -1);
            dE('lang', -1);
            break;
        case "2":
            dE('c_skin', -1);
            dE('lang', 1);
            break;
    }
}
function getFCServer(){
    var obj = document.getElementsByName('fc_extendserver');
    var s = 2;

    if(obj!=null){
        var i;
        for(i=0;i<obj.length;i++){
            if(obj[i].checked){
                s = obj[i].value;
            }
        }
    }
    return s;
}

function getFCClient(){
    var obj = document.getElementsByName('fc_client');
    var s = 0;
    if(obj!=null){
        var i;
        for(i=0;i<obj.length;i++){
            if(obj[i].checked){
                s = obj[i].value;
            }
        }
    }
    return s;
}

function changeMode()
{
    var s = getFCServer();
    var c = getFCClient();
    clientChange(c);
    serverChange(s);
}
