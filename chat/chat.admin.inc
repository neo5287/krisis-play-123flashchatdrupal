<?php
require_once 'lib/functions.php';
require_once 'lib/requestRemote.php';
require_once 'lib/fchatForm.php';
require_once 'language/English.php';

function chat_get_form(){
    $error_Info = "";
    if(isset($_POST['form_id']) && $_POST['form_id'] == "settings_Save")
    {
        $error_Info = settings_Save($_POST);
    }
	$form = array();
    $form = drupal_get_form("settings_Save");
    $form['settings']['type'] = array(
        '#markup' => settings_Disp("",$error_Info),
        '#attached' => array(
            'js' => array(
                        drupal_get_path('module', 'chat') . '/scripts/main_settings.js',
                ),
            ),
        );
    return $form;
}

function chat_Admin_get_form(){
    global $user;
    $server = getParam('fc_extendserver');
    $text = "<div style='text-align:center;'>";
    if($server == 2){
        $text .= <<<INFO
<p>Sorry! Admin Panel is not available in the Free Chat Hosting Edition. 123FlashChat Full Version starts at $30. <br />Click the link below to have a live Admin Panel Demo for testing.</p>
        <p>
        <a href="http://www.123flashchat.com/admin-panel-free.html" target="_blank">Live Demo</a>
        </p>
INFO;
    }else{
        $url =  getParam('fc_client_loc') . "admin_123flashchat.swf?init_host=" . getParam('fc_server_host') . "&init_port=" . getParam('fc_server_port') . (($server == "1") ? ("&init_group=" . getParam('fc_group')) : "");
        $text .= '<script src="'. getParam('fc_client_loc') .'123flashchat.js"></script><object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,19,0" width="700" height="528" id="topcmm_123flashchat" type="application/x-shockwave-flash">
				<param name=movie value="'.$url.'">
				<param name=quality value="high">
				<param name="menu" value="false">
				<param name="allowScriptAccess" value="always">
				<embed src="'.$url.'" quality="high" menu="false" width="700" height="528" type="application/x-shockwave-flash" pluginspace="http://www.macromedia.com/go/getflashplayer"  name="topcmm_123flashchat"  allowScriptAccess="always"></embed>
			</object>';
    }
    $text .= "</div>";
    return $text;
}
function settings_Disp($chat_Config = "",$error_Info = ""){
    $rs = new fchatForm();
    if(!$chat_Config){
        $chat_Config = getConfig();
    }
	$localip = $_SERVER['HTTP_HOST'];
    if(!(function_exists("curl_init") || ini_get("allow_url_fopen"))){
        $error_Info = '<p style="text-align:cener;color:red;">Please contact ISP if "CURL" or "allow_url_fopen" is enabed in PHP configuration.</p>'.$error_Info;
    }
    $loc_client = "http://".$localip.":35555/";
    if( validate_client($loc_client) or validate_client( $chat_Config['fc_api_url'])){
        $valid = "1";
    }else{
        $valid = "0";
    }
    $text = "
        <div id='f_disp' style='text-align:center;'>
"."
<table class='' style='border:1px;'>

<tr>
	<td class='' align='center' colspan='2' style='width:30%; white-space:nowrap;'>".$error_Info."</td>

</tr>
<tr>
        <td class='' align='left'  rowspan='3' style='width:30%; white-space:nowrap;'>".CHAT_SERVER_MODE_CAPTION."</td>
        <td class='' align='left'  style='width:70%;'>".$rs -> getFormRadio("fc_extendserver", 0, $chat_Config['fc_extendserver'] == 0?1:0, "", "onclick=serverChange('0');").CHAT_SERVER_MODE_CAPTION0."</td>
</tr>
<tr>
	<td class='' align='left'  style='width:70%;'>".$rs -> getFormRadio("fc_extendserver", 1, $chat_Config['fc_extendserver'] == 1?1:0, "", "onclick=serverChange('1');").CHAT_SERVER_MODE_CAPTION1."</td>
</tr>
<tr>
	<td class='' align='left'  style='width:70%;'>".$rs -> getFormRadio("fc_extendserver", 2, $chat_Config['fc_extendserver'] == 2?1:0, "", "onclick=serverChange('2');").CHAT_SERVER_MODE_CAPTION2."</td>
</tr>
";

    $text .= "<tr id='s_host'>
                <td class='' align='left'  style='width:30%; white-space:nowrap;'>".CHAT_SERVER_HOST_CAPTION."</td>
                <td class='' align='left'  style='width:70%;'>".$rs -> getFormText("fc_server_host", 50, $chat_Config['fc_server_host'],false,'form-text')."</td>
           </tr> ";
    $text .= "<tr id='s_port'>
                <td class='' align='left'  style='width:30%; white-space:nowrap;'>".CHAT_SERVER_PORT_CAPTION."</td>
                <td class='' align='left'  style='width:70%;'>".$rs -> getFormText("fc_server_port", 6, $chat_Config['fc_server_port'],5,'form-text')."</td>
           </tr> ";
    $text .= "<tr id='s_http_port'>
                <td class='' align='left'  style='width:30%; white-space:nowrap;'>".CHAT_HTTP_PORT_CAPTION."</td>
                <td class='' align='left'  style='width:70%;'>".$rs -> getFormText("fc_http_port", 6, $chat_Config['fc_http_port'],5,'form-text')."</td>
           </tr> ";
    $text .= "<tr id='freedownload'>
        <td class='' align='left'  style='width:30%; white-space:nowrap;'></td>
        <td class='' align='left'  style='width:70%;'><font color='gray'>Note*: Please install 123FlashChat Server first before enable this Mod.<a target='_blank' href='http://www.123flashchat.com/download-now.html?p=123flashchat.exe'>Free Download</a></font></td>
            </tr>";
    $text .= "<tr id='s_info'>
        <td class='' align='left'  style='width:30%; white-space:nowrap;'></td>
        <td class='' align='left'  style='width:70%;'><font color='gray'>".CHAT_CLIENT_DESC."</font></td>
            </tr>";
    $text .= "<tr id='s_fc'>
                <td class='' align='left'  style='width:30%; white-space:nowrap;'>".CHAT_CLIENT_LOCATION_CAPTION."</td>
                <td class='' align='left'  style='width:70%;'>".$rs -> getFormText("fc_client_loc", 50, $chat_Config['fc_client_loc'],false,'form-text')."</td>
           </tr> ";
    $text .= "<tr id='s_free'>
                <td class='' align='left'  style='width:30%; white-space:nowrap;'>".CHAT_ROOM_CAPTION."</td>
                <td class='' align='left'  style='width:70%;'>".$rs -> getFormText("fc_room", 50, $chat_Config['fc_room'],false,'form-text')."</td>
           </tr> ";
    $text .= "<tr id='s_rl'>
                <td class='' align='left'   style='width:30%; white-space:nowrap;'>".CHAT_SHOW_ROOM_LIST_CAPTION."</td>
                <td class='' align='left'   style='width:70%;'>".$rs -> getFormCheckbox("fc_rooms", 1, $chat_Config['fc_room_list']?"checked":"")."</td>
           </tr> ";
    $text .= "<tr>
                <td class='' align='left'  style='width:30%; white-space:nowrap;'>".CHAT_SHOW_USER_LIST_CAPTION."</td>
                <td class='' align='left'   style='width:70%;'>".$rs -> getFormCheckbox("fc_users", 1, $chat_Config['fc_user_list']?"checked":"")."</td>
           </tr> ";
    $text .= "<tr>
                <td class='' align='left'   style='width:30%; white-space:nowrap;'>".CHAT_CLIENT_SIZE_CAPTION."</td>
                <td class='' align='left'   style='width:70%;'>".$rs -> getFormRadio('fc_fullscreen', 0, $chat_Config['fc_fullscreen']?"":"checked").
            CHAT_CLIENT_SIZE_WIDTH.$rs -> getFormText("fc_client_width", 6, $chat_Config['fc_client_width'], 5, 'form-text').
            CHAT_CLIENT_SIZE_HEIGHT.$rs -> getFormText("fc_client_height", 6, $chat_Config['fc_client_height'], 5, 'form-text').
            "</td></tr> ";
    $text .= "<tr id='c_fullscreen'>
                <td class='' align='left'   style='width:30%; white-space:nowrap;'>"."</td>
                <td class='' align='left' style='width:70%;'>".$rs -> getFormRadio("fc_fullscreen", 1, $chat_Config['fc_fullscreen']?"checked":"").CHAT_CLIENT_SIZE_FSC."</td>
           </tr> ";
    $text .= "<tr id='lang'>
                <td class='' align='left' style='width:30%; white-space:nowrap;'>".CHAT_CLIENT_LANGUAGE_CAPTION."</td>
                <td class='' align='left' style='width:70%;'>".$rs -> getFormSelectOpen("fc_client_lang").
            $rs -> getFormOption("Auto detect", "", "auto").
            $rs -> getFormOption("English", $chat_Config["fc_client_lang"] == "en"?"true":"", "en").
            $rs -> getFormOption("GB Chinese", $chat_Config["fc_client_lang"] == "zh-CN"?"true":"", "zh-CN").
            $rs -> getFormOption("Big5 Chinese", $chat_Config["fc_client_lang"] == "en"?"true":"", "zh-TW").
            $rs -> getFormOption("French", $chat_Config["fc_client_lang"] == "fr"?"true":"", "fr").
            $rs -> getFormOption("Italian", $chat_Config["fc_client_lang"] == "it"?"true":"", "it").
            $rs -> getFormOption("German", $chat_Config["fc_client_lang"] == "de"?"true":"", "de").
            $rs -> getFormOption("Dutch", $chat_Config["fc_client_lang"] == "nl"?"true":"", "nl").
            $rs -> getFormOption("Hungarian", $chat_Config["fc_client_lang"] == "hu"?"true":"", "hu").
            $rs -> getFormOption("Spanish", $chat_Config["fc_client_lang"] == "es"?"true":"", "es").
            $rs -> getFormOption("Croatian", $chat_Config["fc_client_lang"] == "hr"?"true":"", "hr").
            $rs -> getFormOption("Turkish", $chat_Config["fc_client_lang"] == "tr"?"true":"", "tr").
            $rs -> getFormOption("Arabic", $chat_Config["fc_client_lang"] == "ar"?"true":"", "ar").
            $rs -> getFormOption("Portuguese", $chat_Config["fc_client_lang"] == "pt"?"true":"", "pt").
            $rs -> getFormOption("Russian", $chat_Config["fc_client_lang"] == "ru"?"true":"", "ru").
            $rs -> getFormOption("Korean", $chat_Config["fc_client_lang"] == "ko"?"true":"", "ko").
            $rs -> getFormOption("Serbian", $chat_Config["fc_client_lang"] == "serbian"?"true":"", "serbian").
            $rs -> getFormOption("Norwegian", $chat_Config["fc_client_lang"] == "no"?"true":"", "no").
            $rs -> getFormOption("Japanese", $chat_Config["fc_client_lang"] == "ja"?"true":"", "ja").
            $rs->getFormSelectClose().
            "<label id='u_al_lab'>".CHAT_CLIENT_LANGUAGE_NOTE."</laber>".
            "</td>
           </tr> ";

    $text .= "<tr id='c_skin'>
                <td class='' align='left'  style='width:30%; white-space:nowrap;'>".CHAT_CLIENT_SKIN_CAPTION."</td>
                <td class='' align='left' style='width:70%;'>".$rs -> getFormSelectOpen("fc_client_skin").
            $rs -> getFormOption("Default", "", "default").
            $rs -> getFormOption("Green", $chat_Config["fc_client_skin"] == "green"?"true":"", "green").
            $rs -> getFormOption("Orange", $chat_Config["fc_client_skin"] == "orange"?"true":"", "orange").
            $rs -> getFormOption("Red", $chat_Config["fc_client_skin"] == "red"?"true":"", "red").
            $rs -> getFormOption("Black", $chat_Config["fc_client_skin"] == "black"?"true":"", "black").
            $rs -> getFormOption("Beige", $chat_Config["fc_client_skin"] == "beige"?"true":"", "beige").
            $rs -> getFormOption("Standard", $chat_Config["fc_client_skin"] == "standard"?"true":"", "standard").
            $rs -> getFormOption("Clean", $chat_Config["fc_client_skin"] == "clean"?"true":"", "clean").
            $rs -> getFormOption("Artistic", $chat_Config["fc_client_skin"] == "artistic"?"true":"", "artistic").
            $rs->getFormSelectClose().
            "<label id='u_as_lab'>".CHAT_CLIENT_SKIN_NOTE."</laber>".
            "</td>
           </tr> ";

    $text .= "<tr>
                <td colspan='2'>".$rs -> getFormButton("submit", "submit", "Save").
            $rs -> getFormButton("reset", "reset", "Reset","onclick=changeMode();").
            "</td>
           </tr> ";
    $text .= "</table>";
    $text .= $rs -> getFormHidden("fc_server_local", $valid);
    // $text .= $rs->getFormClose();
    $text .= getActionJS("main",$chat_Config);
    return $text;
}

function settings_Save($_POST)
{
    if(empty($_POST)){
        return false;
    }
    set_time_limit(300);
    $report_Info = "";
    switch ($_POST['fc_extendserver']){
        case 0:
            $report_Info = save_Local_Settings();
            break;
        case 1:
            $report_Info = save_Host_Settings();
            break;
        default:
            $report_Info = save_Free_Settings();
			break;
    }
    if($report_Info == "1")
    {
        $report_Info = "<p style='text-align:center;color:gray;'>".SAVE_SUCCESSFULLY."</p>";
    }elseif(empty($report_Info)){
        $report_Info = "";
    }
    return $report_Info;
}

function save_Local_Settings(){
    $error_Info = "";
    if(!$_POST){
        return SAVE_FAILED;
    }
    $localip = $_SERVER['HTTP_HOST'];
    $chat_Config['fc_extendserver'] = "0";
    if ($_POST['fc_server_local']){

        $chat_Config['fc_server_host'] = (!empty($_POST['fc_server_host']) && strtolower($_POST['fc_server_host']) != $_SERVER['HTTP_HOST']) ? $_POST['fc_server_host'] : $_SERVER['HTTP_HOST'];
        if(!is_numeric($_POST['fc_server_port'] )||strpos($_POST['fc_server_port'] ,".")!==false){
            $chat_Config['fc_server_port'] = 51127;
        }else{
            $chat_Config['fc_server_port'] = $_POST['fc_server_port'];
        }

        if(!is_numeric($_POST['fc_http_port'] )||strpos($_POST['fc_http_port'] ,".") !== false){
            $chat_Config['fc_http_port'] = 35555;
        }else{
            $chat_Config['fc_http_port'] = $_POST['fc_http_port'];
        }
        $chat_Config['fc_api_url'] = 'http://' . $chat_Config['fc_server_host'] . ':' . $chat_Config['fc_http_port'] . '/';

        if($_POST['fc_server_local']){
            $da = validate_data_api($chat_Config['fc_api_url'] . 'online.js');
        }

        if ($da){
            $error_Info .= "<p style='text-align:center;color:red;'>".SERVER_ERROR."</p>";
            return $error_Info;
        }
    }else{
        $chat_Config['fc_server_host'] = $_SERVER['HTTP_HOST'];
        $chat_Config['fc_server_port'] = 51127;
        $chat_Config['fc_http_port'] = 35555;
        $chat_Config['fc_api_url'] = 'http://' . $chat_Config['fc_server_host'] . ':' . $chat_Config['fc_http_port'] . '/';
        $chat_Config['fc_client_loc'] = $chat_Config['fc_api_url'];
    }
    if(empty ($_POST['fc_client_loc'])){
        $chat_Config['fc_client_loc'] = $chat_Config['fc_api_url'];
    }else{
        $chat_Config['fc_client_loc'] = mysql_escape_string(checkSlash($_POST['fc_client_loc']));
        $chat_Config['fc_client_loc'] .= (substr($chat_Config['fc_client_loc'], 0, 7) == "http://")? "" : "http://";
    }
    $c_own_loc = validate_client($chat_Config['fc_client_loc']);
    if($c_own_loc){
        $error_Info .= "<p style='text-align:center;color:red;'>".CLIENT_ERROR."</p>";
        return $error_Info;
    }
    $chat_Config = check_Public_Settings($chat_Config);
    return save($chat_Config);
}

function save_Host_Settings(){
    $error_Info = "";
    if(!$_POST){
        return SAVE_FAILED;
    }
    $chat_Config['fc_extendserver'] = "1";
    $chat_Config['fc_client_loc'] = $_POST['fc_client_loc'] ? ($_POST['fc_client_loc'] . (substr($_POST['fc_client_loc'],-1,1) != '/' ? '/' : '')) : '';
    $chat_Config['fc_server_host'] = @parse_url($chat_Config['fc_client_loc'], PHP_URL_HOST);
	if ($content = topcmmRequestRemote::requestRemote($chat_Config['fc_client_loc'],1)){
        preg_match('/init_port=([0-9]*)/', $content, $matches);
		if(isset($matches[1])){
			$chat_Config['fc_server_port'] = $matches[1];
		}else{
			$chat_Config['fc_server_port'] = 21127;
		}
    }    
    $chat_Config['fc_group'] = substr(@parse_url($chat_Config['fc_client_loc'], PHP_URL_PATH),1,-1);
	$chat_Config['fc_api_url'] = 'http://' . $chat_Config['fc_server_host'] .'/';
    $c_own_loc = validate_client($chat_Config['fc_client_loc']);
    $da = validate_data_api($chat_Config['fc_api_url'] . 'online.js?group=' . $chat_Config['fc_group']);
	if ($c_own_loc or $da){
        $error_Info .= "<p style='text-align:center;color:red;'>".CLIENT_ERROR."</p>";
        return $error_Info;
    }
    $chat_Config = check_Public_Settings($chat_Config);

    return save($chat_Config);
}

function save_Free_Settings(){
    $error_Info = "";
    if(!$_POST){
        return SAVE_FAILED;
    }
    $chat_Config['fc_extendserver'] = "2";
    $chat_Config['fc_room'] = $_POST['fc_room'] ? $_POST['fc_room'] : "Lobby";
    $chat_Config = check_Public_Settings($chat_Config);
    return save($chat_Config);
}

function check_Public_Settings($chat_Config = array()){
    $chat_Config['fc_room_list'] = isset($_POST['fc_rooms']) ? $_POST['fc_rooms'] : "0";
    $chat_Config['fc_user_list'] = isset($_POST['fc_users']) ? $_POST['fc_users'] : "0";
    $chat_Config['fc_fullscreen'] = $_POST['fc_fullscreen'];
    if(!is_numeric($_POST['fc_client_width'] ) || strpos($_POST['fc_client_width'] ,".")!==false){
        $chat_Config['fc_client_width'] = 700;
    }else{
        $chat_Config['fc_client_width'] = $_POST['fc_client_width'];
    }
    if(!is_numeric($_POST['fc_client_height'] ) || strpos($_POST['fc_client_height'] ,".")!==false){
        $chat_Config['fc_client_height'] = 528;
    }else{
        $chat_Config['fc_client_height'] = $_POST['fc_client_height'];
    }
    $chat_Config['fc_client_lang'] = $_POST['fc_client_lang'];
	$chat_Config['fc_client_skin'] = $_POST['fc_client_skin'];
    return $chat_Config;
}

function save($chat_Config){
    if(!empty ($chat_Config)&& is_array($chat_Config)){
        $sqlStr = "";
        foreach($chat_Config as $key => $val){
            $key = mysql_escape_string($key);
            $val = mysql_escape_string($val);
			if($sqlStr){
				$sqlStr .= ", `$key` = '$val'";
			}else{
				$sqlStr .= "`$key` = '$val'";
			}
        }
        $sqlStr .= " WHERE `id` = '1'";
        $sqlcmd = "update {chat_config} set ".$sqlStr ;
        if(db_query($sqlcmd)){
			return true;
        }
    }
    return false;
}


function getActionJS($type = "main", $chat_Config = ""){
    if(!$chat_Config){
        $chat_Config = getConfig();
    }
    $Js = '';
    if($type == "main"){
        $Js .= '<script type="text/javascript" >
            changeMode();
            dE("f_disp", 1);
    </script>';
    }
    return $Js;
}
?>