<?php
function isValidFilename($filename)
{
	if ($filename ==null)
	{
		return false;
	}
	else if (strlen(trim($filename)) == 0)
	{
		return false;
	}
	else if(strpos($filename, "//") !== false)
	{
		return false;
	}
	else if(strpos($filename, "/") !== false)
	{
		return false;
	}
	else if(strpos($filename,":") !== false)
	{
		return false;
	}
	else if(strpos($filename, "*") !== false)
	{
		return false;
	}
	else if(strpos($filename, "?") !== false)
	{
		return false;
	}
	else if(strpos($filename, "\"") !== false)
	{
		return false;
	}
	else if(strpos($filename, "<") !== false)
	{
		return false;
	}
	else if(strpos($filename, ">") !== false)
	{
		return false;
	}
	else if(strpos($filename, "|") !== false)
	{
		return false;
	}
	else if(strpos($filename, "..") !== false)
	{
		return false;
	}
}

function getImageInfo($filename)
{
	return @getimagesize($filename);
}

function getMimeType($filename)
{
	$imginfo = getImageInfo($filename);
	if(!empty($imginfo))
	{
		return $imginfo["mime"];
	}
	return false;
}

function getFileExt($filename)
{
	return strtolower(substr($filename, strrpos($filename, ".")));
}

function checkFileExtension($filename)
{
	$imginfo = getImageInfo($filename);
	$fileExt = getFileExt($filename);


	if(strtolower($fileExt) == ".jpg" || strtolower($fileExt) == ".jpeg")
	{
		if(!$imginfo && $imginfo["mime"] != "image/jpeg" && $imginfo["mime"]  != "image/pjpeg" && $imginfo["mime"]  != "image/jpg")
		{
			return false;
		}
	}

	if(strtolower($fileExt) == ".png")
	{
		if(!$imginfo && $imginfo["mime"] != "image/png")
		{
			return false;
		}
	}

	if(strtolower($fileExt) == ".bmp")
	{
		if(!$imginfo && $imginfo['mime'] != "image/bmp" && $imginfo['mime'] != "image/x-ms-bmp")
		{
			return false;
		}
	}

	if(strtolower($fileExt) == ".gif")
	{
		if(!$imginfo  && $imginfo['mime'] != "image/gif" )
		{
			return false;
		}
	}
}

function calPercent($getwidth,$getheight,$width,$height)
{
	$rate1 = $width/$getwidth + 0.1;
	$rate2 = $height/$getheight + 0.1;
	$rate = ($rate1 > $rate2) ? $rate1 : $rate2;
	return $rate;
}

function resizeImage($fileName,$im,$imginfo,$rate)
{
	list($width, $height) = getimagesize($fileName);
	$newWidth =  $width/$rate;
	$newHeight = $height/$rate;

	$thumb = imagecreatetruecolor($newWidth, $newHeight);
	imagecopyresized($thumb, $im, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);
	return $thumb;
}

function uploadBinaryFiles($filename, $data)
{
	global $user;
	$file = @fopen($filename, "w");
	@fwrite($file, $data);
	@fclose($file);

	if(file_exists($filename))
	{
		$imginfo = getImageInfo($filename);
		if(!$imginfo && $imginfo["mime"] != "image/jpeg" && $imginfo["mime"]  != "image/pjpeg" && $imginfo["mime"]  != "image/jpg")
		{
			@unlink($filename);
			return false;
		}
		else
		{
			$name = $_GET['fn'];
			$imginfo = getImageInfo($filename);
			$img_end = substr($name,strrpos($name, "."));
			$timestamp = time();
			$filename1 = "picture-" . $user->uid . "-" . $timestamp . $img_end;
			copy($filename, APPLICATION_PATH . "sites/default/files/pictures/".$filename1);
			$picture = $user->picture + 1;
			$sqluser = "update users set picture = " . $picture . " where uid = " . $user->uid;
			$queryuser = db_query($sqluser);
			$size = GetImageSize($filename);
			$sqlfilemanage = "update file_managed set fid = " . $picture . ", timestamp = '" . $timestamp . "', filename = '" . $filename1 . "', uri = 'public://pictures/ " . $filename1 . "', filemime = '" . $imginfo['mime'] . "', filesize = '" . $size . "' where uid = '" . $user->uid . "'";
			$queryfilemanage = db_query($sqlfilemanage);
			return true;
		}
	}
	return false;
}

function addSlash($path)
{
	if(!empty($path))
	{
		$temp = strrev($path);
		if(substr($temp,0,1) != "/" || substr($temp,0,1) != "\\")
		{
			if(false !== strpos($temp, "/"))
			{
				$path .= "/";
			}
			elseif(false !== strpos($temp, "\\"))
			{
				$path .= "\\";
			}
			else
			{
				$path .= "/";
			}
		}
	}
	return $path;
}


function compressimage($file)
{
	if($_GET['cp'] != 1)
	{
		return false;
	}

	$quality    = (!empty($_GET['q']))?$_GET['q']*100:"50";
	$getwidth   = (!empty($_GET['w']))?$_GET['w']:"";
	$getheight  = (!empty($_GET['h']))?$_GET['h']:"";

	$imginfo = getImageInfo();

	list($width, $height) = $imginfo;


	if((!empty($getwidth) && !empty($getheight)) && ($width > $getwidth || $height > $getheight))
	{
		$rate = calPercent($getwidth,$getheight,$width,$height);
	}

	if($imginfo['mime'] == "image/gif")
	{
		$im = imagecreatefromgif($file);
		if(!empty($rate) && $rate > 0)
		{
			$im = resizeImage($file,$im,$imginfo,$rate);
		}
	}
	else if($imginfo['mime'] == "image/png")
	{
		$im=imagecreatetruecolor($width,$height);
		$bg = imagecolorallocate ( $im, 255, 255, 255 );
		imagefill ( $im, 0, 0, $bg );
		$srcimage=imagecreatefrompng($file);

		imagecopyresampled($im,$srcimage,0,0,0,0, $width,$height,$width,$height);

		if(!empty($rate) && $rate > 0)
		{
			$im = resizeImage($file,$im,$imginfo,$rate);
		}
	}
	else if($imginfo['mime'] == "image/jpg" || $imginfo['mime'] == "image/jpeg")
	{

		$im = imagecreatefromjpeg($file);
		if(!empty($rate) && $rate > 0)
		{
			$im = resizeImage($file,$im,$imginfo,$rate);
		}
	}
	else if($imginfo['mime'] == "image/bmp" || $imginfo['mime'] == "image/x-ms-bmp")
	{
		require_once("bmp.php");
		$bmp = new bmp();
		$im =  $bmp->imagecreatefrombmp($file);
		if(!empty($rate) && $rate > 0)
		{
			$im = resizeImage($file,$im,$imginfo,$rate);
		}
	}

	imagejpeg($im,$file,$quality);
}

function uploadfiles($file, $fileName)
{
	global $UPLOAD_SUCCESS,$UPLOAD_ERROR;
	if (@move_uploaded_file($file, $fileName))
	{
		echo $UPLOAD_SUCCESS;
	}
	else
	{
		echo $UPLOAD_ERROR;
	}
}

function getAvatarUploadPath($filepath)
{
	$filepath = addSlash($filepath);

	if(isset($_GET['ulv']))
	{
		$ulv = $_GET['ulv'];
		switch($ulv)
		{
			case "0":
				return getAvatarUploadPathByRole($filepath, "member");
				break;
			case "1":
			  return getAvatarUploadPathByRole($filepath, "guest");
			  break;
		}
	}

	return false;
}

function getAvatarUploadPathByRole($filepath, $role)
{

	$path = $filepath."upload/".$role."/";
	if(!file_exists($path))
	{
		if(!mkdir($path))
		{
			return false;
		}
	}

	return $path;
}
?>