After purchasing the Upload/Webcam Avatar Module, there are two ways to use this module, 
- 123FlashChat HTTP server method,No need to configure anything by using the chat server internal HTTP server to do the job,
- PHP method, Need configuration by using your website language PHP

How to use Upload/Webcam Avatar Module?
1.123FlashChat HTTP server method

(1)By default, the upload file size limit is 2M.

(2)Advanced setting

-- Change upload file size limit
Please open <123flashchat server install directory>/server/etc/fcserver.xml

find 
    <http-server port="35555" maxUpLoadSize="5120" enable="On" documentRoot="..\client" ip="*">
      <file-allow-access></file-allow-access>
    </http-server>
modify maxUPLoad Size, 5120 means 5120=5M, this is a global parameter which is also for image transfer module and file transfer module.

Login 123 Flash Chat Admin Panel ->Module Settings->Upload/Webcam Avatar Module

Please check "Enable Custom Avatar".

Edit Max file size, the unit is KB as well.
Notice, please remeber the Max file size set in admin panel should be smaller than the value in fcserver.xml.

Login 123 Flash Chat Admin Panel ->User Management ->User Group Settings

Please check "Enable Custom Avatar" for each user group if necessary.

-- Enhance Upload/Webcam Avatar Module performance
Since Upload/Webcam Avatar Module will take chat server's usage, so we could modify following configuration to enhance Upload/Webcam Avatar Module performance.

(i)
Open <123flashchat server install directory>/server/etc/fcserver.xml
Find <http-listen-workers>10</http-listen-workers>
Change it to a suitable value between 20 and 50

Find <http-message-handle-workers>10</http-message-handle-workers>
Change it to a suitable value between 20 and 50

(ii)
If you're using Linux/Unix server, adjust the memory allocation for the server as follows:
cd <123flashchat installation directory>/server

vi fcserver.vmoptions
At the last line of this file, there is a symbol "#", remove it and adjust the value followed by the Xmx, then save your adjustments.
./fcserver stop
./fcserver start

-- Notice
(i)Please ensure there's a file called "crossdomain.xml" under "documentRoot" which is defined in fcserver.xml.
(ii)The uploaded file is saved in <123flashchat server installed directory>/client/avatar/default/upload, And this folder should be readable and writeable.
(iii)In admin panel->Module Settings->Upload/Webcam Avatar Module, the Upload URL and Download URL must be left blank if you're using 123FlashChat HTTP server method.
(iv)Please restart chat server if you edit the fcserver.xml.
 

2.PHP method

(1) Configurations
Open "config.php" in the Upload/Webcam Avatar Module package and configure it:

- $MAX_ALLOW_FILE_SIZE: the max upload file size (unit: bytes)

- $FILE_PATH: the upload file path. For security reason it's not supposed to be under the Apache or IIS document root path, 
  			  Because for the security reason, the uploaded files should never be accessed from a HTTP URL directly.
  
  Please also ensure that the $FILE_PATH is readable and writeable by Apache or your webserver.

- $ALLOW_FILE_EXTS: the supported file extension type, e.g., $ALLOW_FILE_EXTS = ".jpeg,.jpg";
				   Leave it blank means allow all kinds of file extension type.	

- $ALLOW_FILE_MIMES: the supported file MIMES for security reason, e.g., $ALLOW_FILE_EXTS = "image/jpeg,image/jpg";
					Leave it blank means allow all kinds of file MIMES.
					
(2). Copy PHP files and crossdomain.xml

Copy all the PHP files in the uploadavatar package to some folder under web server directory, cause they will be needed to access from HTTP address.
Copy the crossdomain.xml in the Upload/Webcam Avatar Module package to the document root folder of the upload avatar HTTP address.

(3). Configure Admin Panel

Login 123 Flash Chat Admin Panel ->Module Settings->Upload/Webcam Avatar Module

Please check "Enable Custom Avatar".

1) Find: Upload URL, configure it to HTTP URL which directs to avatarUploadFile.php,
2) Find: Download URL, configure it to HTTP URL which points to $FILE_PATH which is configured in config.php.

Edit Max file size, the unit is KB as well.
Notice, please remeber the Max file size set in admin panel should be smaller than the value in fcserver.xml.

Login 123 Flash Chat Admin Panel ->User Management ->User Group Settings

Please check "Enable Custom Avatar" for each user group if necessary.

(4) Edit PHP.ini

Find option 'file_uploads', change the value to 'On'.
Fine option 'upload_max_filesize', change the value to same with 'MAX_ALLOW_FILE_SIZE' which is set in config.php.

Then restart Apache or your webserver. 