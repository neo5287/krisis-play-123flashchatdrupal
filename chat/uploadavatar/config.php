<?php

//the max upload file size (unit: bytes)
$MAX_ALLOW_FILE_SIZE= 2048000000;

//the upload file path. For security reason it's not supposed to be under the Apache or IIS document root path, i.e., the uploaded files should never be accessed from a HTTP URL directly.
$FILE_PATH="uploadFiles/";

//the supported file extension type, e.g., $ALLOW_FILE_EXTS = ".jpeg,.jpg";
$ALLOW_FILE_EXTS = ".jpeg,.jpg,.bmp,.png,.gif";

//the supported file MIMES for security reason, e.g., $ALLOW_FILE_EXTS = "image/jpeg,image/jpg";
$ALLOW_FILE_MIMES = "image/jpeg,image/jpg,image/bmp,image/png,image/gif";
?>