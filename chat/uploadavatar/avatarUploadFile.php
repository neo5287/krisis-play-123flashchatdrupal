<?php
function avatar_upload(){
include_once ("config.php");
include_once ("FileUtils.php");
global $user;
$UPLOAD_SUCCESS=0;
$UPLOAD_ERROR=1;
$UPLOAD_OVER_MAX_SIZE=2;
$UPLOAD_FILE_TYPE_ERROR=3;
$filepath = getAvatarUploadPath($FILE_PATH);

$getBinaryData = file_get_contents("php://input");

if(!empty($getBinaryData) && !empty($_GET['fn']))
{
	$filename = $_GET['fn'];
	if(false === isValidFilename($filename))
	{
		echo $UPLOAD_ERROR;
		exit;
	}

	if(file_exists($filepath . $filename))
	{
		if(@unlink($filepath  . $filename))
		{
			uploadBinaryFiles($filepath.$filename, $getBinaryData);
		}
		else{
			echo $UPLOAD_ERROR;
		}
	}
	else
	{
		uploadBinaryFiles($filepath.$filename, $getBinaryData);
	}
}
else
{
	if(empty($_FILES))
	{
		echo $UPLOAD_ERROR;
		exit;
	}

	if(!empty($_FILES["file"]))
	{
		$_FILES["Filedata"] = $_FILES["file"];
	}

	$filebasename = basename($_GET['fn']);
	if(!empty($filebasename))
	{
		$filename = $_GET['fn'];
	}
	else
	{
		$filename = $_FILES["Filedata"]["name"];
	}

	if(false === isValidFilename($filename))
	{
		echo $UPLOAD_ERROR;
		exit;
	}

	if(file_exists($filepath . $filename))
	{
		@unlink($filepath  . $filename);
	}

	$fileSize = $_FILES["Filedata"]["size"];
	$fileType = $_FILES['Filedata']['type'];
	$tmpfile = $_FILES["Filedata"]["tmp_name"];

	if ($fileSize > $MAX_ALLOW_FILE_SIZE)
	{
		echo $UPLOAD_OVER_MAX_SIZE;
		exit;
	}


	if (false === checkFileExtension($tmpfile))
	{
		echo $UPLOAD_FILE_TYPE_ERROR;
		exit;
	}

	$arrowFileExts =  array();
	$arrowFileExts = split(",", $ALLOW_FILE_EXTS);
	if (!empty($arrowFileExts[0]) && !in_array(getFileExt($filename), $arrowFileExts))
	{
		echo $UPLOAD_FILE_TYPE_ERROR;
		exit;
	}

	$arrowFileMines = array();
	$arrowFileMines = split(",", $ALLOW_FILE_MIMES);

	if (!empty($arrowFileMines[0]) && !in_array(getMimeType($tmpfile), $arrowFileMines))
	{
		echo $UPLOAD_FILE_TYPE_ERROR;
		exit;
	}

	if(!file_exists($filepath . $filename))
	{
		compressimage($tmpfile);
		uploadfiles($tmpfile, $filepath.$filename);
	}
	else
	{
		echo $UPLOAD_ERROR;
	}
}

}
?>
