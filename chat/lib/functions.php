<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
*/
require_once("requestRemote.php");
define("CACHE_PATH", "");
function getConfig()
{
    global $chat_Config;
    if(!$chat_Config)
    {
        $sql = "select * from {chat_config} where id='1'";
        $rs = db_query($sql);
        $chat_Config = $rs->fetchAssoc();
        if(!$chat_Config)
        {
            $sqlcmd = "insert into {chat_config} set id = '1'" ;
            db_query($sqlcmd);
            return getConfig();
        }
    }

    return $chat_Config;
}

function getParam($param)
{
    $chat_Config = getConfig();
    return $chat_Config[$param];
}

function checkSlash($path)
{
    $path = trim($path);
    if(substr($path,-1,1) != "/" && !empty($path))
    {
        $path = $path."/";
    }
    return $path;
}

/**
 * Check 123 FlashChat Data api
 * @parm $data_api   Chat Data Api
 * @return integer config status
 */
function validate_data_api($data_api)
{
    $da = 1;
    if(substr(topcmmRequestRemote::requestRemote($data_api,0), 9, 3) == 200)
    {
        $da = 0;
    }

    return $da;
}

/**
 * Check 123 FlashChat Client Location
 * @parm $client_loc   Chat Client Location
 * @return integer config status
 */
function validate_client($client_loc)
{
//    if(empty ($client_loc)){
//        return 1;
//    }
    $c_own_loc = 1;
    $swf = $client_loc . '123flashchat.swf';
    if($headers = @topcmmRequestRemote::requestRemote($swf,0))
    {
        $c_own_loc = (substr($headers, 9, 3) == '200') ? 0 : 1;
    }

    return $c_own_loc;
}


function getChat($chatSetting){
    $chat = "";
    if(!empty ($chatSetting)){
        if($chatSetting['server'] == "2"){
            $chat .= '<script language="javascript" src="'.$chatSetting['url'].'&width='.$chatSetting['width'].'&height='.$chatSetting['height'].'"></script>';
			
        }else
        {
                $chat .=  '<script src="'.$chatSetting['fc_client_loc'].'123flashchat.js"></script><object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,19,0" id="topcmm_123flashchat"  width="'.$chatSetting['width'].'" height="'.$chatSetting['height'].'">
                    <param name=movie value="'.$chatSetting['url'].'">
                    <param name=quality value="high">
                    <param name="menu" value="false">
                    <param name="allowScriptAccess" value="always">
                    <embed src="'.$chatSetting['url'].'" allowScriptAccess="always" quality="high" menu="false" width="'.$chatSetting['width'].'" height="'.$chatSetting['height'].'" type="application/x-shockwave-flash" pluginspace="http://www.macromedia.com/go/getflashplayer"  name="topcmm_123flashchat"></embed>
                </object>';
        }
    }

    return $chat;
}


function getChatSettings($chat_Config = "", $_GET = "", $user = "")
{
    $url = "";
    if(!is_array($chat_Config) || empty ($chat_Config))
    {
        $chat_Config = getConfig();
    }
    if($chat_Config)
    {
        $c_full = getParam("fc_fullscreen");
        $server = getParam('fc_extendserver');
        $lang = getParam('fc_client_lang');
        $skin = getParam('fc_client_skin');
        $fc_client_loc = getParam('fc_client_loc');
        $fc_client_width = getParam('fc_client_width')."px";
        $fc_client_height = getParam('fc_client_height')."px";
		
        if ($server != "2")
        {
            $client_name = '123flashchat.swf';
            $url = getParam('fc_client_loc') . $client_name . "?init_host=" . getParam('fc_server_host') . "&init_port=" . getParam('fc_server_port') ;
            $url .= ($server == "1") ? ("&init_group=" . getParam('fc_group')) : "";
        }
        else
        {
            $url = "http://free.123flashchat.com/js.php?room=" . getParam('fc_room') . "&skin="  . $skin . "&lang="  . $lang;
        }
        $room = isset($_GET['room'])?$_GET['room']:"";
        if ($room)
        {
            $url .= "&init_room=" . $room;
        }
        if ($user)
        {
            if(!empty($user->name) && !empty($user->pass) )
            {
                $url .= "&init_user=" . rawurlencode($user->name) . "&init_password=" . rawurlencode($user->pass);
            }
        }
    }else
    {
        return false;
    }
    $chatSetting = array (
            "server"=>$server,
            'url' => $url,
			'fullscreen' => $c_full,
            'width' => $fc_client_width,
            'height' => $fc_client_height,
    		'fc_client_loc'  => $fc_client_loc,
    );

    return $chatSetting;
}

function serviceGetChatStats()
{

    $sOutputCode = '';
    $c_full = getParam("fc_fullscreen");

    if($c_full)
    {
        $fc_client_width = "screen.width";
        $fc_client_height = "screen.height";
    }else
    {
        $fc_client_width = getParam('fc_client_width');
        $fc_client_height = getParam('fc_client_height');
    }
    $status = getStatus();
    if (getParam('fc_user_list'))
    {
        $users  = getUsers();
    }
    if ((getParam('fc_room_list')) && (getParam('fc_extendserver') != "2"))
    {

        $rooms  = getRooms();
    }

    if (isset($status['ln']))
    {
        $sOutputCode .= '<b>' . $status['ln'] . '</b> login users.';
        if (isset($status['rn']))
        {
            $sOutputCode = '<b>' . $status['rn'] . '</b> rooms, ' . '<b>' . $status['cn'] . '</b> connections, ' . $sOutputCode;
        }
    }
    if(isset($rooms))
    {
        $sOutputCode .= "<b><p>Rooms:</b> ";
        if(!$rooms)
        {
            $sOutputCode .= "None";
        }
        else
        {
            $xa = '';

            foreach($rooms as $room)
            {
                if(!empty($GLOBALS['conf']['clean_url'])){
                	 $sOutputCode .= $xa . '<a href="'.'chat?room=' . $room['id'] . '" target="_blank">' .$room['name'] .'</a>(' . $room['count'] . ')';
                }else{
                	$sOutputCode .= $xa . '<a href="'.'?q=chat&room=' . $room['id'] . '" target="_blank">' .$room['name'] .'</a>(' . $room['count'] . ')';
                }
                    $xa = ', ';
            }
        }

    }
    if(isset($users))
    {
        $sOutputCode .= "</p><p><b>Users:</b> ";
        if(!$users)
        {
            $sOutputCode .= "None</p>";
        }
        else
        {
            $xb = '';
            foreach($users as $user)
            {
                $sOutputCode .= $xb . $user['name'];
                $xb = ', ';
            }
            $sOutputCode .= "</p>";
        }

    }

	 if(!empty($GLOBALS['conf']['clean_url'])){
	 	$sOutputCode .=  '<p><a href="http://www.123flashchat.com" onclick="window.open(\'chat?room=\');return false;"><b>Chat Now!</b></a>';
	 }else{
   		$sOutputCode .=  '<p><a href="http://www.123flashchat.com" onclick="window.open(\'?q=chat&room=\');return false;"><b>Chat Now!</b></a>';
	 }


    $sOutputCode = '<div class="dbContentHtml">' . $sOutputCode . '</div>';
    return $sOutputCode;

}

function getStatus()
{

    $data = topcmmRequestRemote::requestRemote(CACHE_PATH . "status.js",1);
    $status_json = substr($data,10);
    if ((time() > substr($data,0,10)) || !$status_json)
    {
        $server =  getParam('fc_extendserver');
        switch ($server)
        {
            case "0":
                $status_js = getParam('fc_api_url') . "online.js";
                if($rs = topcmmRequestRemote::requestRemote($status_js,1))
                {
                    $status_json = substr($rs,11,-1);
                }
                break;
            case "1":
                $status_js = getParam('fc_api_url') . "online.js?group=" . getParam('fc_group');
                if($rs = topcmmRequestRemote::requestRemote($status_js,1))
                {
                    $status_json = substr($rs,11,-1);
                }
                break;
            case "2":
                $status_js = "http://free.123flashchat.com/freeroomnum.php?roomname=" . getParam('fc_room');
                if($rs = topcmmRequestRemote::requestRemote($status_js,1))
                {
                    preg_match("/document.write\('(.*)'\);/",$rs,$matches);
                    $status['ln'] = $matches[1];
                    $status_json = json_encode($status);
                }
                break;
        }
        @file_put_contents(CACHE_PATH . "status.js",(time() + 120) . $status_json);
    }
    return json_decode($status_json,true);
}

function getRooms()
{

    $data = topcmmRequestRemote::requestRemote("../".CACHE_PATH ."rooms.js", 1);
    $rooms_json = substr($data,10);
    if ((time() > substr($data,0,10)) || !$rooms_json)
    {
        $server =  getParam('fc_extendserver');
        switch ($server)
        {
            case "0":
                $room_js = getParam('fc_api_url') . "rooms.js";
                break;
            case "1":
                $room_js = getParam('fc_api_url') . "rooms.js?group=" . getParam('fc_group');
                break;
        }
        if($rs = topcmmRequestRemote::requestRemote($room_js,1))
        {
            $rooms_json = substr($rs,10,-1);
        }
        @file_put_contents(CACHE_PATH ."rooms.js",(time() + 120) . $rooms_json);
    }
    return json_decode($rooms_json, true);
}
/**
 * Function will get users;
 */
function getUsers()
{
    $context = stream_context_create(array(
            'http' => array(
                    'timeout' => 3      // Timeout in seconds
            )
    ));
    $data =  topcmmRequestRemote::requestRemote(CACHE_PATH ."cache/users.js",1);
    $users_json = substr($data,10);
    if ((time() > substr($data,0,10)) || !$users_json)
    {
        $server =  getParam('fc_extendserver');
        switch ($server)
        {
            case "0":
                $rooms = getRooms();
                $users = array();
                foreach ($rooms as $room)
                {
                    $user_js = getParam('fc_api_url') . "roomonlineusers.js?roomid=" . $room['id'];
                    if($rs = topcmmRequestRemote::requestRemote($user_js,1))
                    {
                        $users = array_merge($users, json_decode(substr($rs,20,-1),true));
                    }
                }
                $users_json = json_encode($users);
                break;
            case "1":
                $user_js = getParam('fc_api_url') . "roomonlineusers.js?group=" . getParam('fc_group');
                if($rs = topcmmRequestRemote::requestRemote($user_js,1))
                {
                    $users_json = substr($rs,20,-1);
                }
                break;
            case "2":
                $user_js = "http://free.123flashchat.com/freeroomuser.php?roomname=" . getParam('fc_room');

                if($rs = topcmmRequestRemote::requestRemote($user_js,1))
                {
                    preg_match("/document.write\('(.*)'\);/",$rs,$matches);
                    foreach (explode(',', $matches[1]) as $user)
                    {
                        $users[] = array('name' => $user);
                    }
                }
                $users_json = json_encode($users);
                break;
        }
		$users_json = filterUserList($users_json);		
        @file_put_contents(CACHE_PATH ."users.js",(time() + 120) . $users_json);
    }
    return json_decode($users_json, true);
}

function filterUserList($users_json)
{
	$user_array =  json_decode($users_json,true);
	$user_regroup_array = array();
	$user_tmp_array = array();
	foreach($user_array as $key => $val)
	{
		if (!in_array($val['name'], $user_tmp_array))
		{
			$user_tmp_array[] = $val['name'];
			$user_regroup_array[$key] = $user_array[$key];
		}
	}
	return json_encode($user_regroup_array);
}
?>
