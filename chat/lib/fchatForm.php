<?php
class fchatForm
{

    function getFormOpen($formMethod, $formAction, $formName = "", $formTarget = "", $formEnctype = "", $formJs = "")
    {
        $name = ($formName ? " id='".$formName."' " : " id='chat'");
        $method = ($formMethod ? "method='".$formMethod."'" : "");
        $target = ($formTarget ? " target='".$formTarget."'" : "");

        return "\n<form action='".$formAction."' ".$method.$target.$name.$formEnctype.$formJs.">";
    }

    function getFormText($formName, $formSize, $formValue, $formMaxlength = FALSE, $formClass = "", $formReadonly = "", $formTooltip = "", $formJs = "")
    {
        $name = ($formName ? " id='".$formName."' name='".$formName."'" : "");
        $value = (isset($formValue) ? " value='".$formValue."'" : "");
        $size = ($formSize ? " size='".$formSize."'" : "");
        $maxlength = ($formMaxlength ? " maxlength='".$formMaxlength."'" : "");
        $readonly = ($formReadonly ? " readonly='readonly'" : "");
        $tooltip = ($formTooltip ? " title='".$formTooltip."'" : "");
        return "\n<input class='".$formClass."' type='text' ".$name.$value.$size.$maxlength.$readonly.$tooltip.$formJs." />";
    }

    function getFormPassword($formName, $formSize, $formValue, $formMaxlength = FALSE, $formClass = "", $formReadonly = "", $formTooltip = "", $formJs = "")
    {
        $name = ($formName ? " id='".$formName."' name='".$formName."'" : "");
        $value = (isset($formValue) ? " value='".$formValue."'" : "");
        $size = ($formSize ? " size='".$formSize."'" : "");
        $maxlength = ($formMaxlength ? " maxlength='".$formMaxlength."'" : "");
        $readonly = ($formReadonly ? " readonly='readonly'" : "");
        $tooltip = ($formTooltip ? " title='".$formTooltip."'" : "");
        return "\n<input class='".$formClass."' type='password' ".$name.$value.$size.$maxlength.$readonly.$tooltip.$formJs." />";
    }

    function getFormButton($formType, $formName, $formValue, $formJs = "", $formImage = "", $formTooltip = "")
    {
        $name = ($formName ? " id='".$formName."' name='".$formName."'" : "");
        $image = ($formImage ? " src='".$formImage."' " : "");
        $tooltip = ($formTooltip ? " title='".$formTooltip."' " : "");
        return "\n<input class='form-submit' type='".$formType."' ".$formJs." value='".$formValue."'".$name.$image.$tooltip." />";
    }

    function getFormTextarea($formName, $formColumns, $formRows, $formValue, $formJs = "", $formStyle = "", $formWrap = "", $formReadonly = "", $formTooltip = "")
    {
        $name = ($formName ? " id='".$formName."' name='".$formName."'" : "");
        $readonly = ($formReadonly ? " readonly='readonly'" : "");
        $tooltip = ($formTooltip ? " title='".$formTooltip."'" : "");
        $wrap = ($formWrap ? " wrap='".$formWrap."'" : "");
        $style = ($formStyle ? " style='".$formStyle."'" : "");
        return "\n<textarea class='' cols='".$formColumns."' rows='".$formRows."' ".$name.$formJs.$style.$wrap.$readonly.$tooltip.">".$formValue."</textarea>";
    }

    function getFormCheckbox($formName, $formValue, $formChecked = 0, $formTooltip = "", $formJs = "")
    {
        $name = ($formName ? " id='".$formName.$formValue."' name='".$formName."'" : "");
        $checked = ($formChecked ? " checked='checked'" : "");
        $tooltip = ($formTooltip ? " title='".$formTooltip."'" : "");
        return "\n<input type='checkbox' value='".$formValue."'".$name.$checked.$tooltip.$formJs." />";

    }

    function getFormRadio($formName, $formValue, $formChecked = 0, $formTooltip = "", $formJs = "")
    {
        $name = ($formName ? " id='".$formName.$formValue."' name='".$formName."'" : "");
        $checked = ($formChecked ? " checked='checked'" : "");
        $tooltip = ($formTooltip ? " title='".$formTooltip."'" : "");
        return "\n<input type='radio' value='".$formValue."'".$name.$checked.$tooltip.$formJs." />";

    }

    function getFormFile($formName, $formSize, $formTooltip = "", $formJs = "")
    {
        $name = ($formName ? " id='".$formName."' name='".$formName."'" : "");
        $tooltip = ($formTooltip ? " title='".$formTooltip."'" : "");
        return "<input type='file' class='' size='".$formSize."'".$name.$tooltip.$formJs." />";
    }

    function getFormSelectOpen($formName, $formJs = "")
    {
        return "\n<select id='".$formName."' name='".$formName."' class='' ".$formJs." >";
    }

    function getFormSelectClose()
    {
        return "\n</select>";
    }

    function getFormOption($formOption, $formSelected = "", $formValue = "", $formJs = "")
    {
        $value = ($formValue !== FALSE ? " value='".$formValue."'" : "");
        $selected = ($formSelected ? " selected='selected'" : "");
        return "\n<option".$value.$selected." ".$formJs.">".$formOption."</option>";
    }

    function getFormHidden($formName, $formValue)
    {
        return "\n<input type='hidden' id='".$formName."' name='".$formName."' value='".$formValue."' />";
    }

    function getFormClose()
    {
        return "\n</form>";
    }
}

?>
